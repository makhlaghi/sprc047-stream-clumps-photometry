# Makefile to estimate the noise level of point-like sources in the
# Spitzer/IRAC images of SPRC047; as described in Section 3.2 of
# https://arxiv.org/abs/2312.05358 (titled "Stellar Population
# Properties in the Stellar Streams Around SPRC047").
#
# Usage:
#    make INDIR=/path/to/input/dataset \
#         BDIR=/path/to/build/directory -j8
#
# By default this script will not do the deconvolution, but simply use
# already deconvolved data for the fully reproducible images it
# creates (described under "Input data" below). This is because the
# AWMLE code is not yet publicly released and was run directly by
# Roberto Baena-Galle (co-author of this project) and provided as the
# tarball within the input data.
#
# Dependencies: GNU Astronomy Utilites (Gnuastro).
#    http://www.gnu.org/software/gnuastro/
# Gnuastro installation instructions are available here:
#    http://www.gnu.org/s/gnuastro/manual/html_node/Quick-start.html
#
# Input data:
# There are two types of input data for this project:
#   - Data that are specific to this project and should be downloaded
#     from: https://doi.org/10.5281/zenodo.10397812 . These files
#     should be placed in the 'INDIR' mentioned above.
#   - A general dataset (the raw Spitzer/IRAC extended warp PSF) which
#     does not belong to the authors of this project. If not already
#     present in 'INDIR', it will be downloaded automatically (value
#     to 'rawpsfurl' variable below; which is a tarball, from that the
#     file with the name of the 'rawpsffile' variable is used).
#
# Copyright (C) 2020-2023 Mohammad Akhlaghi <mohammad@akhlaghi.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.





# Input files: the raw PSF can be obtained from the public URL
# below. It should then be put in the same directory as the processed
# image, but under a 'irac-extended-psf' sub-directory.
rawpsffile = I1_hdr_warm_psf.fits
deconvimg=$(INDIR)/deconvolved-it75.fits
rawinput = $(INDIR)/sprc047_3.6um-bck.fits
rawpsfurl = https://irsa.ipac.caltech.edu/data/SPITZER/docs/irac/calibrationfiles/psfprf/Warm.IRAC.Extended.PSF.5X.091113.tar.gz

# Processing parameters.
imgsize = 501
numrandom = 100
psfname = psf-0.6.fits

# Width of PSF in its original resolution
psfwidth = 151

# The raw noise: after convolving with the correlated noise the
# standard deviation of pixels with no original signal should be
# similar to the input dataset's noise level (0.0020).
rawnoise = 0.0065

# The size of the mock profile (FWHM if its Gaussian, effective radius
# if its Sersic).
profsize = 5

# The truncation radius of the mock profile (as a multiple of 'profsize').
proftrunc = 5

# FWHM of Gaussian profile to simulate correlated noise.
corrnoisefwhm = 2

# Final (after convolution, and sampling to output) maximum value of
# mock profile.
final3x3sum = 0.663138

# Aperture radius for measurements (in pixels)
aperture-rad = 5

# Width of crop to show in report.
width-crop = 50





# Make sure the two BDIR and INDIR environment variables are set. When
# building the report, we are defining the final target as a '.PHONY'
# so the final PDF is always built which is good for LaTeX '\ref's to
# be (re)constructed, even when the dependencies have not changed.
ifeq ($(words $(INDIR) $(BDIR)),2)
all: report.pdf
.PHONY: report.pdf
else
all:
	@echo "These environment variables are necessary for this Makefile:"
	echo "  INDIR: directory containing 'seppo' and 'roberto' subdirs."
	echo "  BDIR: directory to host temporary built products"
	echo "        (this directory will be built if it doesn't exist,"
	echo "         we recommend using an empty directory)."
	echo "for example:"
	echo "  make INDIR=/path/to/input/dataset BDIR=/path/to/build/directory"
	echo ""; exit 1
endif

# Basic settings for the shell that Make calls.
.ONESHELL:
SHELL:=/bin/bash
.SHELLFLAGS = -ec

# Make the build directory
$(BDIR):; mkdir $@





# Scale the PSF to the actual image pixel resolution and crop it to
# the requested size.
psf=$(BDIR)/psf.fits
$(psf): | $(BDIR)

        # Download the file if it is not in 'INDIR'.
	rawpsf=$(BDIR)/$(rawpsffile)
	if [ -f $(INDIR)/$(rawpsffile) ]; then
	  cp $(INDIR)/$(rawpsffile) $(BDIR)/
	else
	  psfdir=$(BDIR)/psf-downloaded
	  if ! [ -d $$psfdir ]; then mkdir $$psfdir; fi
	  cd $$psfdir
	  wget -O psf-downloaded.tar.gz $(rawpsfurl)
	  tar -xf psf-downloaded.tar.gz
	  cd ..
	  cp $$psfdir/$(rawpsffile) ./
	  rm -r $$psfdir
	fi

        # Find the difference in scale between the PSF and the input
        # image.
	psinput=$$(astfits $(rawinput) -h0 --pixelscale -q \
	                   | awk '{print $$1}')
	pspsf=$$(astfits $$rawpsf -h0 --pixelscale -q \
	                 | awk '{print $$1}')
	scale=$$(echo "$$pspsf $$psinput" | awk '{print $$1/$$2}')

        # Scale the PSF into the image size.
	psfscaled=$(BDIR)/psf-scaled.fits
	astwarp $$rawpsf -h0 --scale=$$scale -o$$psfscaled

        # Crop out the desired part. Note that due to the particular
        # PSF that is downloaded above, after warping, the largest
        # value is shifted one pixel down in the y direction.
	psfnn=$(BDIR)/psf-not-normalized.fits
	center=$$(astfits $$psfscaled -h1  \
	                  | awk '/NAXIS1/{x=int($$3/2)+1} \
	                         /NAXIS2/{y=int($$3/2)+1} \
	                         END{printf "%g,%g",x,y-1}')
	astcrop $$psfscaled --mode=img --center=$$center \
	        --width=$(psfwidth) -o$$psfnn

        # Normalize the PSF (divide it by its sum).
	astarithmetic $$psfnn set-i i i sumvalue / -o$@

        # Clean up.
	rm $$psfscaled $$psfnn





# Build the no-noised mock image in the PSF resolution, convolve with
# the PSF and then warp to the input dataset's scale.
nonoise = $(BDIR)/no-noise.fits
$(nonoise): $(rawinput) $(psf) | $(BDIR)

        # Build the mock image, and convolve it.
	export GSL_RNG_SEED=1603665025
	cat=$(BDIR)/no-noise-pre-conv-cat.txt
	preconv=$(BDIR)/no-noise-pre-conv-not-scaled.fits
	ccenter=$$(echo $(imgsize) | awk '{print $$1 - 100}')
	center=$$(echo $(imgsize) | awk '{print int($$1/2)+1}')
	echo "1 $$center   $$center   3 $(profsize) 0 0 1 1   $(proftrunc)"  > $$cat
	echo "1 80         200        1 5           4 0 1 500 $(proftrunc)" >> $$cat
	echo "1 $$ccenter  $$ccenter  1 8           6 0 1 500 $(proftrunc)" >> $$cat
	astmkprof $$cat --mergedsize=$(imgsize),$(imgsize) \
	          --psfinimg --envseed --oversample=1 \
	          --mcolissum -o$$preconv

        # Convolve the mock image with the PSF.
	notscaled=$(BDIR)/no-noise-noscaled.fits
	astconvolve $$preconv --domain=spatial -o$$notscaled \
	            --kernel=$(psf)

        # Crop the image center in a 3x3 box to measure the total value:
	scalecrop=$(BDIR)/no-noise-crop.fits
	astcrop $$notscaled --center=$$center,$$center --mode=img \
	        --width=3,3 --output=$$scalecrop

        # Scale the image before and after convolution so the maximum
        # value after convolution corresponds to the requested maximum
        # value in the input image (because we don't know the full
        # brightness).
	nonoise=$(BDIR)/no-noise-pre-conv.fits
	frac=$$(aststatistics $$scalecrop --sum \
	                      | awk '{print $(final3x3sum)/$$1}')
	astarithmetic $$preconv $$frac x -o$$nonoise
	astarithmetic $$notscaled $$frac x -o$@
	rm $$scalecrop $$notscaled





# Kernel to simulate correlated noise
kernel=$(BDIR)/kernel.fits
$(kernel): | $(BDIR)
	astmkprof --kernel=gaussian,$(corrnoisefwhm),5 --oversample=1 -o$@





# Add noise
ndir=$(BDIR)/noised
$(ndir): | $(BDIR); mkdir $@
noised = $(foreach i,$(shell seq $(numrandom)),$(ndir)/$(i).fits)
$(noised): $(ndir)/%.fits: $(nonoise) $(kernel) | $(ndir)

        # Add the noise to the no-noise image.
	export GSL_RNG_SEED=$*
	rawnoise=$(ndir)/$*-noised-raw.fits
	astarithmetic $(nonoise) $(rawnoise) mknoise-sigma --envseed \
	              -o$$rawnoise

        # Convolve the noised image with the kernel to simulate
        # correlated noise.
	astconvolve $$rawnoise --kernel=$(kernel) --domain=spatial -o$@

        # To check the noise level. We'll first set all the non-zero
        # pixels of 'nonoise' to NaN, then get the noise standard
        # deviation. Currently, we want a noise-level (after
        # accounting for correlated noise) of ~0.002.
        #astarithmetic $@ $(nonoise) 0 ne nan where stdvalue -g1; exit 1

        # To visually check the image.
        #ds9-multi-ext $@; exit 1

        # Clean up (keep the first one for demonstration).
	if [ $* != 1 ]; then rm $$rawnoise; fi





# Do the deconvolution
# --------------------
#
# So far, the deconvolution is done by Roberto's method that is not
# yet public. So I am importing the images he provided.
ddir=$(BDIR)/deconvolved
$(ddir): | $(BDIR); mkdir $@
deconv = $(subst $(ndir),$(ddir),$(noised))
$(deconv): $(ddir)/%.fits: $(ndir)/%.fits | $(ddir)
        # If you just want to generate the inputs to the deconvolution
        # test, then uncomment (remove the '#' from) the '#echo' line
        # below.

	#echo; echo "READY FOR DECONVOLUTION"; echo; exit 1
	cd $(ddir)
	origname=$$(printf "%03d" $*)_deconv_A.fits
	tar -xf $(INDIR)/deconvolved-simulated.tar.gz \
	    deconvolved-simulated/$$origname --strip-components=1
	cp $$origname $@
	rm $$origname





# Aperture label for measurement.
aper=$(BDIR)/aperture.fits
$(aper): $(nonoise) $(deconv)
#	Smaller aperture on deconvolved images.
	c=$$(astfits $(ddir)/1.fits -h0 | awk '/^NAXIS1/{print int($$3/2)+1}')
	echo "1 $$c $$c 5 $(aperture-rad) 0 0 1 1 1" \
	     | astmkprof --background=$(ddir)/1.fits --backhdu=0 \
	                 --clearcanvas --oversample=1 --mforflatpix \
	                 --type=uint8 -o$(BDIR)/aperture-dconv.fits -q

#	Full-size aperture
	c=$$(astfits $(nonoise) -h1 | awk '/^NAXIS1/{print int($$3/2)+1}')
	echo "1 $$c $$c 5 $(aperture-rad) 0 0 1 1 1" \
	     | astmkprof --background=$(nonoise) --clearcanvas \
	                 --oversample=1 --mforflatpix --type=uint8 -o$@




# Measurement over the given aperture.
cdir=$(BDIR)/catalog
nncat=$(cdir)/no-noise.txt
$(cdir): | $(BDIR); mkdir $@
cat = $(subst .fits,.txt,$(subst $(ndir),$(cdir),$(noised)))
$(cat): $(cdir)/%.txt: $(ddir)/%.fits $(aper) | $(cdir)
	tmp=$(cdir)/$*-tmp.fits
	astmkcatalog $(BDIR)/aperture-dconv.fits -h1 --sum -o$$tmp \
	             --valueshdu=0 --valuesfile=$(BDIR)/deconvolved/$*.fits
	asttable $$tmp | awk '{printf "%-10s%.3f\n", $*, $$1}' > $@
	rm $$tmp
$(nncat): $(cdir)/%.txt: $(BDIR)/%.fits $(aper) | $(cdir)
	tmp=$(cdir)/$*-tmp.fits
	astmkcatalog $(aper) -h1 --sum -o$$tmp --valueshdu=1 \
	             --valuesfile=$(BDIR)/$*-pre-conv.fits
	asttable $$tmp | awk '{printf "%-10s%.3f\n", "$*", $$1}' > $@
	rm $$tmp





# Calculate the information to plot.
texdir=$(BDIR)/tex
$(texdir): | $(BDIR); mkdir $@
histogram = $(texdir)/histogram.txt
$(histogram): $(cat) | $(texdir)
	cat $(cat) | aststatistics -c2 --numbins=50 --histogram -o$@





# Demo images to show in report.
itexdir = $(texdir)/images
$(itexdir): | $(texdir); mkdir $@
demoimgs = $(itexdir)/done.text
$(demoimgs): $(deconv) | $(itexdir)

        # Calculate the center.
	crop=$(itexdir)/crop.fits
	center=$$(astfits $(nonoise) -h1 \
	                  | awk '/^NAXIS1/{print int($$3/2)+1}')

#	First crop before finding the maximum value.
	astcrop $(BDIR)/no-noise-pre-conv.fits --mode=img -o$$crop \
	        --center=$$center,$$center --width=$(width-crop)

        # Set the high value in the visualization (to help in visualization).
	flow=-0.003
	fhigh=$$(aststatistics $$crop --maximum)
	options="--fluxlow=$$flow --fluxhigh=$$fhigh --forcemin --forcemax"
	options="$$options --colormap=sls"

        # Image before convolution.
	astconvertt $$crop $$options -o$(itexdir)/no-noise-pre-conv.pdf

        # Image after convolution (with a separate crop)
	astcrop $(BDIR)/no-noise.fits --mode=img -o$$crop \
	        --center=$$center,$$center --width=$(width-crop)
	astconvertt $$crop $$options -o$(itexdir)/no-noise.pdf

        # Image with ideal noise.
	astcrop $(ndir)/1-noised-raw.fits --mode=img -o$$crop \
	        --center=$$center,$$center --width=$(width-crop)
	astconvertt $$crop $$options -o$(itexdir)/1-noise-raw.pdf

        # Image with correlated noise.
	astcrop $(ndir)/1.fits --mode=img -o$$crop \
	        --center=$$center,$$center --width=$(width-crop)
	astconvertt $$crop $$options -o$(itexdir)/1-noise-correlated.pdf

        # Crop from the real data. Note that the raw input image
	astcrop $(rawinput) -h0 --mode=wcs --center=209.9103974,25.0276965 \
	        --widthinpix --width=$(width-crop) -o$$crop.fits
	astarithmetic $$crop.fits 0.002 + -o$$crop
	astconvertt $$crop $$options -o$(itexdir)/1-real.pdf

        # Examples after deconvolution
	center=$$(astfits $(ddir)/1.fits -h0 \
	                  | awk '/^NAXIS1/{print int($$3/2)+1}')
	for i in $$(seq 5); do
	  astcrop $(ddir)/$$i.fits -h0 --mode=img -o$$crop \
	          --center=$$center,$$center --width=$(width-crop)
	  astconvertt $$crop $$options -o$(itexdir)/$$i-deconv.pdf
	done

#	Build final target.
	rm -f $$crop $$crop.fits
	echo "done" > $@





# Cutouts comparing convolved and deconvolved images.
cutoutwidth=1.5
center="209.9239020,25.0126333"
cutouts = $(texdir)/images/deconvolved.jpg
$(cutouts): $(rawinput) $(deconvimg) | $(itexdir)

        # The flux limits:
	options="--fluxlow=-0.01 --fluxhigh=0.1 --colormap=sls"
	options="$$options --forcemin --forcemax"

        # Actual input.
	crop=$(subst .jpg,-crop.fits,$@)
	astcrop $(rawinput) -h0 --mode=wcs --center=$(center) \
	        --width=$(cutoutwidth)/60 --zeroisnotblank -o$$crop
	astconvertt $$crop $$options --output=$(subst .jpg,-in.jpg,$@)
	cp $$crop $(subst .jpg,-input.fits,$@)

        # Deconvolved image.
	warp=$(subst .jpg,-warp.fits,$@)
	astwarp $(deconvimg) -h1 -o$$warp
	astcrop $$warp -h1 --mode=wcs --center=$(center) \
	        --width=$(cutoutwidth)/60 --zeroisnotblank -o$$crop
	astconvertt $$crop $$options --output=$@
	cp $$crop $(subst .jpg,-deconv.fits,$@)

        # Clean up.
	rm $$crop $$warp





# Report the values to be included in the text.
macros = $(texdir)/macros.tex
$(macros): $(cat) $(nncat) $(cutouts) | $(texdir)

        # Report basic settings.
	macros=$(texdir)/macros.tex
	echo "\newcommand{\imgsize}{$(imgsize)}" > $$macros
	echo "\newcommand{\inputunits}{MJy/sr}" >> $$macros
	echo "\newcommand{\numrandom}{$(numrandom)}" >> $$macros
	echo "\newcommand{\psfwidth}{$(psfwidth)}" >> $$macros
	echo "\newcommand{\rawnoise}{$(rawnoise)}" >> $$macros
	echo "\newcommand{\profsize}{$(profsize)}" >> $$macros
	echo "\newcommand{\proftrunc}{$(proftrunc)}" >> $$macros
	echo "\newcommand{\corrnoisefwhm}{$(corrnoisefwhm)}" >> $$macros
	echo "\newcommand{\finalthreebythreesum}{$(final3x3sum)}" >> $$macros
	echo "\newcommand{\aperturerad}{$(aperture-rad)}" >> $$macros
	echo "\newcommand{\widthcrop}{$(width-crop)}" >> $$macros
	echo "\newcommand{\rawpsfurl}{$(rawpsfurl)}" >> $$macros
	echo "\newcommand{\cutoutwidtharcmin}{$(cutoutwidth)}" >> $$macros

        # Report Git commit and its date.
	commit=$$(git describe --dirty --always --long)
	date=$$(git show -s --format=%ai HEAD | awk '{print $$1}')
	echo "\newcommand{\gitcommit}{$$commit}" >> $$macros
	echo "\newcommand{\gitcommitdate}{$$date}" >> $$macros

        # Pixel scale of input.
	v=$$(astfits $(rawinput) -h0 --pixelscale -q \
	             | awk '{printf "%.2f", $$1*3600}')
	echo "\newcommand{\inputpixelscale}{$$v}" >> $$macros

        # Report the reference measurement.
	ref=$$(awk '{print $$2}' $(nncat))
	echo "\newcommand{\aperreference}{$$ref}" >> $$macros

        # Measure basic distribution properties.
	mms=$$(cat $(cat) | aststatistics -c2 --mean --median --std)
	std=$$(echo $$mms | awk '{printf "%.2f", $$3}')
	mean=$$(echo $$mms | awk '{printf "%.2f", $$1}')
	median=$$(echo $$mms | awk '{printf "%.2f", $$2}')
	echo "\newcommand{\apermedian}{$$median}" >>  $$macros
	echo "\newcommand{\apermean}{$$mean}"     >> $$macros
	echo "\newcommand{\apermstd}{$$std}"      >> $$macros





# Build the final report
report.pdf: report.tex $(macros) $(demoimgs) $(histogram)
	curdir=$$(pwd)
	cd $(texdir)
	if ! [ -d tikz ]; then mkdir tikz; fi
	pdflatex -shell-escape -halt-on-error $$curdir/report.tex
	cp report.pdf $$curdir
