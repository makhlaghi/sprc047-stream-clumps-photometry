\documentclass[a4paper]{article}

%% Include the macros
\input{macros.tex}

%% Set the print size.
\usepackage[a4paper, includeheadfoot, body={18.7cm, 24.5cm}]{geometry}

%% To include images.
\usepackage{graphicx}

%% To allow links.
\usepackage[
  colorlinks,
  urlcolor=blue,
  citecolor=blue,
  linkcolor=blue,
  linktocpage]{hyperref}
\renewcommand\UrlFont{\rmfamily}

%% To draw the figures with PGFPlots.
\usepackage{tikz}
\usepackage{pgfplots}
\pgfplotsset{compat=newest}
\usepgfplotslibrary{groupplots}
\pgfplotsset{
  axis line style={thick},
  tick style={semithick},
  tick label style = {font=\footnotesize},
  every axis label = {font=\footnotesize},
  legend style = {font=\footnotesize},
  label style = {font=\footnotesize}
  }



%% Set the title and author.
\title{Correlated noise and its effect on deconvolution}
\author{Mohammad Akhlaghi, Roberto Baena-Gall\'e, Seppo Laine}
\date{(Commit \gitcommit{} at \gitcommitdate)}









\begin{document}
\maketitle

\noindent
In this short report, we want to measure the error in deconvolution that is caused by correlated noise.
To do this, first we made a mock Gaussian profile (with a FWHM of $\profsize$ pixels, trunctated at $\proftrunc\times$FWHM).
It is shown in Figure \ref{fig-err-intro} (left-most image).
We then convolve the image with the Warm IRAC Extended PSF\footnote{\url{\rawpsfurl}. This raw PSF was scaled to a pixel scale of $\inputpixelscale$ arcsec/pixel (same as the analysis image we are using). Also, because of the small size of our mock image ($\imgsize\times\imgsize$), only its central $\psfwidth\times\psfwidth$ region was used.}: Figure \ref{fig-err-intro} (second image).
Furthermore, to allow calibration of the deconvolved image, two very bright stars were placed on very distant positions from this test profile such that after convolution they do not contribute at all to the pixel under study here.

However, the main science target we are studying is heavily contaminated by the wings of the nearby galaxy, setting the profile's ``total'' wouldn't give an accurate result, because its hard to measure (and the purpose of all this work!).
Therefore, after convolving the profile with the PSF, we scaled the convolved image (multiplied by a constant), such that within a $3\times3$ pixel box around the center, it has the same pixel sum as the main target in the observed image ($\finalthreebythreesum$ \inputunits).

To simulate correlated noise, we first created an ideal noise with a fixed $\sigma$ (where each pixel's noise is not affected by its nearby pixels) as shown in Figure \ref{fig-err-intro} (3).
Afterwards, we convolved the noised image with a sharp Gaussian kernel (FWHM$=$\corrnoisefwhm) (Figure \ref{fig-err-intro}, 4).
The $\sigma$ value of the ideal noise was set like this: we know the noise of the final observed image (0.0020 \inputunits), so the ideal noise was manually changed such that after convolving the image with the sharp Gaussian kernel, the noise would be similar to the observed level (this can be automated later!).
After several tries, the selected value was: $\sigma=\rawnoise$.

\begin{figure}[h]
  \begin{center}
    \includegraphics[width=0.195\linewidth]{images/no-noise-pre-conv.pdf}
    \includegraphics[width=0.195\linewidth]{images/no-noise.pdf}
    \includegraphics[width=0.195\linewidth]{images/1-noise-raw.pdf}
    \includegraphics[width=0.195\linewidth]{images/1-noise-correlated.pdf}
    \includegraphics[width=0.195\linewidth]{images/1-real.pdf}
  \end{center}
  \vspace{-7mm}
  \caption{\label{fig-err-intro}\small The input profile. From left to right: 1) profile before convolution. 2) after convolution. 3) after adding raw noise. 4) after convolving the raw noised image  with a sharp kernel to simulate correlated noise. 5) a crop from a relatively empty region of the image with same crop size near SPRC047 (to show the similarity of the real and mock correlated noise). All images have the same color scale.}
\end{figure}

Having found the ideal noise $\sigma$, we simulated correlated noise on the convolved image $\numrandom$ times.
Each time, just changing the random number generator seed from 1 to $\numrandom$ (so the exact same noise pattern can be reproduced later).
These 100 images were then fed into the deconvolution algorithm and the results of the first five can be visually seen in Figure \ref{fig-err-demo}.

\begin{figure}[h]
  \begin{center}
    \includegraphics[width=0.195\linewidth]{images/1-deconv.pdf}
    \includegraphics[width=0.195\linewidth]{images/2-deconv.pdf}
    \includegraphics[width=0.195\linewidth]{images/3-deconv.pdf}
    \includegraphics[width=0.195\linewidth]{images/4-deconv.pdf}
    \includegraphics[width=0.195\linewidth]{images/5-deconv.pdf}
  \end{center}
  \vspace{-7mm}
  \caption{\label{fig-err-demo}\small Five example deconvolved images, set to the same color scale as the images of Figure \ref{fig-err-intro}.}
\end{figure}

To measure the accuracy of measurements on the deconvolved image, we measured the brightness within an aperture of radius $\aperturerad$ pixels.
First this was measured on the image without any noise and \emph{before convolution}, or Figure \ref{fig-err-intro} (1), and measured to be $\aperreference$ \inputunits.
Then it was applied to the $\numrandom$ deconvolved images and the result was $\apermedian\pm\apermstd$ \inputunits.
The full distribution of measurements can be seen in Figure \ref{histogram}.

%% The histogram.
\begin{figure}[h]
  \begin{center}
    \begin{tikzpicture}
      \begin{axis}[no markers,
          axis on top,
          xmode=normal,
          ymode=normal,
          yticklabels={},
          scale only axis,
          xlabel=Brightness within $\aperturerad$ pixel aperture,
          width=0.9\linewidth,
          height=0.3\linewidth,
          enlarge y limits=false,
          enlarge x limits=false,
        ]
        \addplot [const plot mark mid, fill=red, opacity=0.7]
        table [x index=0, y index=1]
        {histogram.txt}
        \closedcycle;
      \end{axis}
    \end{tikzpicture}
  \end{center}
  \vspace{-7mm}
  \caption{\label{histogram}\small Histogram of measured brightness values. With median and standard deviation of $\apermedian\pm\apermstd$.}
\end{figure}

Figure \ref{imagedeconv} shows the input and deconvolved images.

\begin{figure}[h]
  \begin{center}
    \includegraphics[width=0.35\linewidth]{images/deconvolved-in.jpg}
    \includegraphics[width=0.35\linewidth]{images/deconvolved.jpg}
  \end{center}
  \vspace{-7mm}
  \caption{\label{imagedeconv}\small Input (left) and deconvolved image (right).}
\end{figure}


\end{document}
