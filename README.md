Measurement of uncertainty in deconvolution (from Laine et al. 2024)
====================================================================

This project contains the necessary source files to reproduce the
analysis on estimating the error bars of measurements on a deconvolved
image. The result of this work was published as Section 3.2 of
[arXiv:2312.05358](https://arxiv.org/abs/2312.05358).

To execute this script, you need Gnuastro 0.21, as well as the
necessary input data that are described in the comments at the top of
the `Makefile` in this project. Afterwards, simply run the following
command (while correcting the values of `INDIR` and `BDIR`, or
changing the number of CPU threads with the `-j` option).

```shell
$ make INDIR=/path/to/input/dataset -j8 \
       BDIR=/path/to/build/directory
```

For more on the execution details and input and build directories,
please see the comments at the top of the `Makefile`.
